import 'dart:io';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //Zsuzsanna made changes
  // This widget is the root of your application.
  var addText = [
    'Zsuzsanna Dianovics',
    'Fall 2020',
    'A ship in harbor is safe, but that is not what ships are for.'
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weolcome to CS481',
      home: Scaffold(
        backgroundColor: Colors.yellow,
        appBar: AppBar(
          title: Text('Welcome to our class'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                addText[0],
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: (30)),
              ),
              Text(
                addText[1],
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: (20)),
              ),
              Text(
                addText[2],
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
